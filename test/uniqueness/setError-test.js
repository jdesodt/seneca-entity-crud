/* Copyright (c) 2018 e-soa Jacques Desodt, MIT License */
'use strict'

/* Prerequisites */
const processUniqueness = require('../../process/uniqueness')
const testFunctions = require('../functions')

/* Test prerequisites */
const Code = require('code')
const Lab = require('lab', {timeout: testFunctions.timeout})
const lab = (exports.lab = Lab.script())
const after = lab.after
const describe = lab.describe
const it = lab.it
const expect = Code.expect

/* Backups functions before mock */
const backups = new Map()

describe('uniqueness setError', function () {
  after((done) => {
    /* Restores the origin functions */
    for (var [key, values] of backups) {
      values.forEach(function (item) { key[item[0]] = item[1] })
    }
    done()
  })
  /* Test */
  it('ok', function (fin) {
    var fieldName = 'f2'
    /* Fires the test */
    var result = processUniqueness.setError(getOptions(), getEntity(), fieldName)
    /* Checks the result */
    expect(result.field).to.equal(fieldName)
    expect(result.actual).to.equal(getEntity().f2)
    expect(result.error).to.equal(getOptions().msg_not_unique)
    fin()
  })
})

/* ---------- FUNCTIONS ---------- */

function getEntity () {
  return { f1: 'n1', f2: 'n2', f3: 'n3' }
}

function getOptions () {
  return { msg_not_unique: 'not unique' }
}
