/* Copyright (c) 2018 e-soa Jacques Desodt, MIT License */
'use strict'

/* Prerequisites */
const processUniqueness = require('../../process/uniqueness')
const testFunctions = require('../functions')

/* Test prerequisites */
const Code = require('code')
const Lab = require('lab', {timeout: testFunctions.timeout})
const lab = (exports.lab = Lab.script())
const after = lab.after
const describe = lab.describe
const it = lab.it
const expect = Code.expect

/* Backups functions before mock */
const backups = new Map()

describe('uniqueness setSelect', function () {
  after((done) => {
    /* Restores the origin functions */
    for (var [key, values] of backups) {
      values.forEach(function (item) { key[item[0]] = item[1] })
    }
    done()
  })
  /* Test */
  it('empty index', function (fin) {
    /* Fires the test */
    var result = processUniqueness.setSelect(getEntity(), [])
    /* Checks the result */
    expect(result).to.equal({})
    fin()
  })
  it('full index', function (fin) {
    var index = ['f2', 'f3']
    /* Fires the test */
    var result = processUniqueness.setSelect(getEntity(), index)
    /* Checks the result */
    expect(result).to.equal({ f2: 'n2', f3: 'n3'})
    fin()
  })
})

/* ---------- FUNCTIONS ---------- */

function getEntity () {
  return { f1: 'n1', f2: 'n2', f3: 'n3' }
}
