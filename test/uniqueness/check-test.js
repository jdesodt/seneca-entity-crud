/* Copyright (c) 2018 e-soa Jacques Desodt, MIT License */
'use strict'

/* Prerequisites */
const processUniqueness = require('../../process/uniqueness')
const testFunctions = require('../functions')

/* Test prerequisites */
const Code = require('code')
const Lab = require('lab', {timeout: testFunctions.timeout})
const lab = (exports.lab = Lab.script())
const after = lab.after
const describe = lab.describe
const it = lab.it
const expect = Code.expect

/* Backups functions before mock */
const backups = new Map()
backups.set(processUniqueness, [
  ['checkIndex', processUniqueness.checkIndex.bind({})]
])

describe('uniqueness check', function () {
  after((done) => {
    /* Restores the origin functions */
    for (var [key, values] of backups) {
      values.forEach(function (item) { key[item[0]] = item[1] })
    }
    done()
  })
  /* No argument */
  it('no args', function (fin) {
    /* Fires the test */
    processUniqueness.check(null, null, null, {}, null)
    .then(function (error) {
      /* Checks the result */
      expect(error).to.not.exist()
      fin()
    })
  })
  /* Argument empty */
  it('empty args', function (fin) {
    /* Fires the test */
    processUniqueness.check(null, null, null, { uniqueness: [] }, { f1: 'n1' })
    .then(function (error) {
      /* Checks the result */
      expect(error).to.not.exist()
      fin()
    })
  })
  /* With arguments */
  it('args ok, error', function (fin) {
    var msg = 'Ooops!'
    /* Mocks */
    mockCheckIndexError(msg)
    /* Fires the test */
    processUniqueness.check(null, null, null, getArgs(), { f1: 'n1' })
    .catch(function (err) {
      expect(err.message).to.equal(msg)
      fin()
    })
  })
  it('args ok, unique', function (fin) {
    /* Mocks */
    mockCheckIndexUnique()
    /* Fires the test */
    processUniqueness.check(null, null, null, getArgs(), { f1: 'n1' })
    .then(function (error) {
      /* Checks the result */
      expect(error).to.not.exist()
      fin()
    })
  })
  it('args ok, not unique', function (fin) {
    /* Mocks */
    mockCheckIndexNotUnique()
    /* Fires the test */
    processUniqueness.check(null, null, getOptions(), getArgs(), { f1: 'n1' })
    .then(function (error) {
      /* Checks the result */
      expect(error).to.exist()
      expect(error.field).to.equal('f1')
      fin()
    })
  })
})

/* ---------- MOCKS ---------- */

function mockCheckIndexError (msg) {
  processUniqueness.checkIndex = function (seneca, act, options, args, entity, index) {
    return new Promise(function (resolve, reject) {
      return reject(new Error(msg))
    })
  }
  return false
}

function mockCheckIndexNotUnique () {
  processUniqueness.checkIndex = function (seneca, act, options, args, entity, index) {
    return new Promise(function (resolve, reject) {
      return resolve({field: index[0], actual: 'jobi joba', error: options.msg_not_unique})
    })
  }
  return false
}

function mockCheckIndexUnique () {
  processUniqueness.checkIndex = function (seneca, act, options, args, entity, index) {
    return new Promise(function (resolve, reject) {
      return resolve(null)
    })
  }
  return false
}

/* ---------- FUNCTIONS ---------- */

function getArgs () {
  return { uniqueness: [ ['f1', 'f2'], ['f3'] ] }
}

function getOptions () {
  return { msg_not_unique: 'not unique' }
}
