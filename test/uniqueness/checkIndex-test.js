/* Copyright (c) 2018 e-soa Jacques Desodt, MIT License */
'use strict'

/* Prerequisites */
const processQuery = require('../../process/query')
const processUniqueness = require('../../process/uniqueness')
const testFunctions = require('../functions')

/* Test prerequisites */
const Code = require('code')
const Lab = require('lab', {timeout: testFunctions.timeout})
const lab = (exports.lab = Lab.script())
const after = lab.after
const describe = lab.describe
const it = lab.it
const expect = Code.expect

/* Backups functions before mock */
const backups = new Map()
backups.set(processQuery, [
  ['query', processQuery.query.bind({})]
])

describe('uniqueness checkIndex', function () {
  after((done) => {
    /* Restores the origin functions */
    for (var [key, values] of backups) {
      values.forEach(function (item) { key[item[0]] = item[1] })
    }
    done()
  })
  /* No argument */
  it('no args', function (fin) {
    /* Fires the test */
    processUniqueness.checkIndex(null, null, null, null, null, null)
    .then(function (error) {
      /* Checks the result */
      expect(error).to.not.exist()
      fin()
    })
  })
  /* Query */
  it('query error', function (fin) {
    var msg = 'Ooops!'
    /* Mocks */
    mockQueryError(msg)
    /* Fires the test */
    processUniqueness.checkIndex(null, null, getOptions(), getArgs(), getEntity(), getIndex(0))
    .catch(function (err) {
      /* Checks the result */
      expect(err.message).to.equal(msg)
      fin()
    })
  })
  it('query unique', function (fin) {
    /* Mocks */
    mockQueryUnique()
    /* Fires the test */
    processUniqueness.checkIndex(null, null, getOptions(), getArgs(), getEntity(), getIndex(0))
    .then(function (error) {
      /* Checks the result */
      expect(error).to.not.exist()
      fin()
    })
  })
  it('query not unique', function (fin) {
    /* Mocks */
    mockQueryNotUnique()
    /* Fires the test */
    processUniqueness.checkIndex(null, null, getOptions(), getArgs(), getEntity(), getIndex(0))
    .then(function (error) {
      /* Checks the result */
      expect(error).to.exist()
      expect(error.field).to.equal('f1')
      fin()
    })
  })
})

/* ---------- MOCKS ---------- */

function mockQueryError (msg) {
  processQuery.query = function (seneca, act, options, args, done) {
    return done(new Error(msg))
  }
  return false
}

function mockQueryUnique () {
  processQuery.query = function (seneca, act, options, args, done) {
    return done(null, {
      success: true,
      list: [],
      count: 0
    })
  }
  return false
}

function mockQueryNotUnique () {
  processQuery.query = function (seneca, act, options, args, done) {
    return done(null, {
      success: true,
      list: [{ id: 'i1' }, { id: 'i2' }],
      count: 2
    })
  }
  return false
}

/* ---------- FUNCTIONS ---------- */

function getArgs () {
  return {
    entity: { f1: 'n1', f2: 'n2', f3: 'n3' },
    uniqueness: [ ['f1', 'f2'], ['f3'] ] }
}

function getEntity () {
  return getArgs().entity
}

function getIndex (i) {
  return getArgs().uniqueness[i]
}

function getOptions () {
  return { msg_not_unique: 'not unique' }
}
