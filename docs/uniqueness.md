# uniqueness

Last update: 06/14/2018

## Description

This feature is part of the [seneca-entity-crud][] plugin. It provides uniqueness control on some fields before creating or updating an entity.

# How it works

## When and where to use it

The **uniqueness** feature is optional and applies only to the [create][] and [update][] commands.

## The pattern

The pattern is:

```js
uniqueness: [ ['fieldname_1', 'fieldname_2', ...], [ ... ], ...]
```

The **uniqueness** value is an array of arrays of fields names. Each array of fields names can be seen as an index in the database. The process will query on each index before creating or updating. If some rows are found, the command return a response with the error.

## Example

Let's use a microservice managing Users. Its database contains:

```json
[
    { id: 'abc', firstname: 'John', lastname: 'Doo', phone: '0102030405' },
    { id: 'def', firstname: 'Jack', lastname: 'Doo', phone: '0203040506' },
    { id: 'xyz', firstname: 'Jack', lastname: 'Robinson' }
]
```

We must ensure that the following fields have unique values in the database:

* firstname **and** lastname: we cannot accept two `John Doo` in the database.
* phone: we cannot accept two `0203040506` in the database.

### create

With this pattern

```json
{ role: 'my role', cmd: 'create', uniqueness: [['firstname', 'lastname'], ['phone']], entity: myNewEntity }
```

the [seneca-entity-crud][] plugin will do the work for you. In our example, the results will be:

| entity to create                                             | result                                                       |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| { firstname: 'John', lastname: 'Doo', phone: '1234' }        | { success: false, errors: [ { field: 'firstname', actual: 'John', error: 'not unique' } ]} |
| { firstname: 'Bill', lastname: 'Boket', phone: '0102030405' } | { success: false, errors: [ { field: 'phone', actual: '0102030405', error: 'not unique' } ]} |
| { firstname: 'John', lastname: 'Niss', phone: '007' }        | { success: true, errors: [], entity: { id: 'ijk', firstname: 'John', lastname: 'Niss', phone: '007' } } |

> Note: If the uniqueness (index) argument contains at least two field names, the error object will contain only the first field checked.

### update

With this pattern

```json
{ role: 'my role', cmd: 'update', uniqueness: [['firstname', 'lastname'], ['phone']], entity: myEntity }
```

the [seneca-entity-crud][] plugin will do the work for you. In our example, the results will be:

| entity to update                                             | result                                                       |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| { id: 'abc', firstname: 'John', lastname: 'Doo', phone: '0203040506' } | { success: false, errors: [ { field: 'phone', actual: '0203040506', error: 'not unique' } ]} |
| { id: 'def', firstname: 'John', phone: '987654' }            | { success: false, errors: [ { field: 'firstname', actual: 'John', error: 'not unique' } ]} |
| { id: 'xyz', firstname: 'John', phone: '007' }               | { success: true, errors: [], entity: { id: 'xyz', firstname: 'John', lastname: 'Robinson', phone: '007' } } |

> Note: If the uniqueness (index) argument contains at least two field names, the error object will contain only the first field checked.

## Result object

The uniqueness feature returns:

* **null**: if the uniqueness check is OK.
* **an error object**: if the uniqueness check fails. This object pattern is:

```json
{ field: 'a field name', actual: 'the field value', error: 'the error message' }
```

> Note: If the uniqueness (index) argument contains at least two field names, the error object will contain only the first field checked.

# Contributing

The [Senecajs org][] encourages open participation. If you feel you can help in any way, be it with documentation, examples, extra testing, or new features please get in touch.

## License

Copyright (c) 2018, Richard Rodger and other contributors.
Licensed under [MIT][].

[create]: https://gitlab.com/jdesodt/seneca-entity-crud/blob/master/docs/crud-create.md
[seneca-entity-crud]: https://gitlab.com/jdesodt/seneca-entity-crud
[update]: https://gitlab.com/jdesodt/seneca-entity-crud/blob/master/docs/crud-update.md
