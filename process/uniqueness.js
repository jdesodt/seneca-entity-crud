/* Copyright (c) 2018 e-soa Jacques Desodt, MIT License */
'use strict'

/* Prerequisites */
const processQuery = require('./query')
const promise = require('bluebird')

var processUniqueness = {}

/* Checks the uniqueness of arrays of fields */
processUniqueness.check = function (seneca, act, options, args, entity) {
  return new Promise(function (resolve, reject) {
    /* Initializes */
    var error = null
    /* Checks the uniqueness argument */
    if (args.uniqueness && args.uniqueness.length > 0) {
      /* Initializes */
      var promises = []
      /* Sets the promises */
      for (var i = 0; i < args.uniqueness.length; i++) {
        promises.push(processUniqueness.checkIndex(seneca, act, options, args, entity, args.uniqueness[i]))
      }
      /* Runs the promises */
      if (promises.length > 0) {
        promise.all(promises)
        .then(function (results) {
          if (results) {
            /* Loops on each check result */
            for (var i = 0; i < results.length; i++) {
              if (results[i]) {
                error = results[i]
                break
              }
            }
          }
          return resolve(error)
        })
        .catch(function (err) { return reject(err) })
      /* No promises to run */
      } else { return resolve(null) }
    /* No uniqueness argument */
    } else { return resolve(null) }
  })
}

/* Checks the uniqueness of one array of fields */
processUniqueness.checkIndex = function (seneca, act, options, args, entity, index) {
  return new Promise(function (resolve, reject) {
    /* Checks the index value */
    if (index && index.length > 0) {
      /* Sets the query arguments */
      var queryArgs = {
        zone: args.zone ? args.zone : options.zone,
        base: args.base ? args.base : options.base,
        name: args.name ? args.name : options.name,
        select : processUniqueness.setSelect(entity, index)
      }
      /* Executes the query */
      processQuery.query(seneca, act, options, queryArgs, function (err, result) {
        if (err) { return  reject(err) }
        /* We must check the IDs in case of an update command */
        var another = false
        for (var i = 0; i < result.count; i++) {
          if (result.list[i].id !== entity.id) {
            another = true
            break
          }
        }
        if (another) {
          return resolve(processUniqueness.setError(options, entity, index[0]))
        } else { return resolve(null) }
      })
    /* No index */
    } else { return resolve(null) }
  })
}

/* Sets the error message */
processUniqueness.setError = function (options, entity, fieldName) {
  return {field: fieldName, actual: entity[fieldName], error: options.msg_not_unique}
}

/* Sets the query select argument */
processUniqueness.setSelect = function (entity, index) {
  /* Initializes */
  var select = {}
  /* Loops on the fields */
  for (var i = 0; i < index.length; i++) {
    select[index[i]] = entity[index[i]]
  }
  return select
}

/* Exports this plugin */
module.exports = processUniqueness
