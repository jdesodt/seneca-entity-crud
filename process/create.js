/* Copyright (c) 2018 e-soa Jacques Desodt, MIT License */
'use strict'

const processUniqueness = require('./uniqueness')

var processCreate = {}

/* CRUD Create: new entity persistence
   If the 'last_update' option is set to true, the field 'last_update'
   is set on current date and added to the entity before insert. */
processCreate.create = function (seneca, act, options, args, done) {
  /* Initializes */
  var errors = args.errors ? args.errors : []
  /* Checks if the entity is passed */
  if (!args.entity) {
    errors.push({field: null, actual: null, error: options.msg_no_entity})
    done(null, {success: false, errors: errors})
  } else {
    /* Checks the uniqueness */
    processUniqueness.check(seneca, act, options, args, args.entity)
    .then(function (error) {
      /* Checks if the error exists */
      if (error) {
        errors.push(error)
        done(null, {success: false, errors: errors})
      } else {
        /* Gets the entity */
        var entity = args.entity
        /* Checks if the last update date has to be set */
        if (options.last_update) {
          entity.last_update = Date.now()
        }
        /* Gets the namespace */
        var zone = args.zone ? args.zone : options.zone
        var base = args.base ? args.base : options.base
        var name = args.name ? args.name : options.name
        /* Saves the entity in the database */
        var entityFactory = seneca.make$(zone, base, name)
        entityFactory.save$(entity, function (err, entity) {
          if (err) { throw err }
          /* Checks if the namespace has to be removed */
          if (args.nonamespace || args.nonamespace === 'true') {
            /* Removes the seneca field
               Don't use delete entity.entity$ (cause it throws an error) */
            delete entity['entity$']
          }
          /* Returns the new entity with its id set */
          done(null, {success: true, errors: [], entity: entity})
        })
      }
    })
    .catch(function (err) { throw err })
  }
}

/* Exports this plugin */
module.exports = processCreate
